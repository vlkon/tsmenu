#!/usr/bin/env python3
"""
a helper script to list all available font names for the tkinter
"""

from tkinter import Tk
import tkinter.font
Tk()
for name in sorted(tkinter.font.families()):
    print(name)
